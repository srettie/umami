MC Samples
==============

The recommended h5 ntuples and the derivations which have been used for dumping them using the training-dataset-dumper are listed in the [FTAG algorithm documentation samples overview](https://ftag.docs.cern.ch/software/samples/).

