parameters:
  # Path where the ntuples are saved
  ntuple_path: &ntuple_path /share/rcifdata/srettie/FTAG_training_h5/

  # Path where the hybrid samples will be saved
  sample_path: &sample_path /share/rcifdata/srettie/FTAG_preprocessing/full_dips_loose/hybrids/

  # Path where the merged and ready-to-train samples are saved
  file_path: &file_path /share/rcifdata/srettie/FTAG_preprocessing/full_dips_loose/preprocessed/

preparation:
  ntuples:
    ttbar:
      path: *ntuple_path
      file_pattern: user.alfroch.410470.btagTraining.e6337_e5984_s3126_r12629_p4724.EMPFlow_loose.2021-09-09-T005957-R30414_output.h5/*.h5

    zprime:
      path: *ntuple_path
      file_pattern: user.alfroch.800030.btagTraining.e7954_s3672_r12629_r12636_p4724.EMPFlow_loose.2021-09-09-T005957-R30414_output.h5/*.h5

  samples:
    training_ttbar_bjets:
      type: ttbar
      category: bjets
      n_jets: 10e6
      n_split: 10
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 0
        pt_cut:
          operator: <=
          condition: 2.5e5
        HadronConeExclTruthLabelID:
          operator: ==
          condition: 5
      f_output:
        path: *sample_path
        file: MC16d_hybrid-bjets_even_1_PFlow-merged.h5
      merge_output: f_tt_bjets

    training_ttbar_cjets:
      type: ttbar
      category: cjets
      # Number of c jets available in MC16d
      n_jets: 5048271
      n_split: 13
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 0
        pt_cut:
          operator: <=
          condition: 2.5e5
        HadronConeExclTruthLabelID:
          operator: ==
          condition: 4
      f_output:
        path: *sample_path
        file: MC16d_hybrid-cjets_even_1_PFlow-merged.h5
      merge_output: f_tt_cjets

    training_ttbar_ujets:
      type: ttbar
      category: ujets
      n_jets: 20e6
      n_split: 20
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 0
        pt_cut:
          operator: <=
          condition: 2.5e5
        HadronConeExclTruthLabelID:
          operator: ==
          condition: 0
      f_output:
        path: *sample_path
        file: MC16d_hybrid-ujets_even_1_PFlow-merged.h5
      merge_output: f_tt_ujets

    training_ttbar_taujets:
      type: ttbar
      category: taujets
      n_jets: 12745953
      n_split: 5
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 0
        pt_cut:
          operator: <=
          condition: 2.5e5
        HadronConeExclTruthLabelID:
          operator: ==
          condition: 15
      f_output:
        path: *sample_path
        file: MC16d_hybrid-taujets_even_1_PFlow-merged.h5
      merge_output: f_tt_taujets

    training_zprime:
      type: zprime
      n_jets: 9193312
      n_split: 2
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 0
        pt_cut:
          operator: ">"
          condition: 2.5e5
      f_output:
        path: *sample_path
        file: MC16d_hybrid-ext_even_0_PFlow-merged.h5
      merge_output: f_z

    testing_ttbar:
      type: ttbar
      n_jets: 4e6
      n_split: 2
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 1
      f_output:
        path: *sample_path
        file: MC16d_hybrid_odd_100_PFlow-no_pTcuts.h5

    testing_zprime:
      type: zprime
      n_jets: 4e6
      n_split: 2
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 1
      f_output:
        path: *sample_path
        file: MC16d_hybrid-ext_odd_0_PFlow-no_pTcuts.h5

    testing_zprime_highpt:
      type: zprime
      n_jets: 4e6
      n_split: 2
      cuts:
        eventNumber:
          operator: mod_2_==
          condition: 1
        pt_cut:
          operator: ">"
          condition: 4.0e5
      f_output:
        path: *sample_path
        file: MC16d_hybrid-ext_odd_0_PFlow-highpt.h5

# amount of b-jets from ttbar which are used in the pre-resampling,
# can change after applying resampling in the hybrid sample creation
njets: 2194900

# fraction of ttbar jets wrt. Z'
# can change after applying resampling in the hybrid sample creation
ttbar_frac: 0.65

# Whether or not to enforce the ttbar fraction above
enforce_ttbar_frac: False

# outputfiles are split into 5
# iterations: 1
iterations: 5

# pT cut for hybrid creation (for light and c-jets)
pTcut: 2.5e5

# pT cut for b-jets
bhad_pTcut: 2.5e5

# upper pT limit for all jets
pT_max: False

# set to true if taus are to be included in preprocessing
bool_process_taus: False

# set to true if extended flavour labelling scheme is used in preprocessing
bool_extended_labelling: False

# Define undersampling method used. Valid are "count", "weight",
# "count_bcl_weight_tau", "template_b" and "template_b_count"
# count_bcl_weight_tau is a hybrid of count and weight to deal with taus.
# template_b uses the b as the target distribution, but does not guarantee
# same fractions. template_b_count will guarantee same fractions.
# The "template_b" and "template_b_count" does not work well with taus as of now.
# Additionally, when computing the target distribution for "template_b" and "template_b_count",
# pT_max (set above) will be used to compute the probability ratios or PDFs.
# Default is "count".
# See RunUndersampling in preprocessing for more info
sampling_method: count

# Name of the output files for the different jets
# ZPrime (Mixed)
f_z:
  path: *file_path
  file: MC16d_hybrid-ext_even_0_PFlow-merged.h5

# ttbar (b)
f_tt_bjets:
  path: *file_path
  file: MC16d_hybrid-bjets_even_1_PFlow-merged.h5

# ttbar (c)
f_tt_cjets:
  path: *file_path
  file: MC16d_hybrid-cjets_even_1_PFlow-merged.h5

# ttbar (u)
f_tt_ujets:
  path: *file_path
  file: MC16d_hybrid-ujets_even_1_PFlow-merged.h5

# ttbar (tau)
f_tt_taujets:
  path: *file_path
  file: MC16d_hybrid-taujets_even_1_PFlow-merged.h5

# Name of the output file from the preprocessing
outfile_name: /share/rcifdata/srettie/FTAG_preprocessing/full_dips_loose/preprocessed/PFlow-hybrid.h5
plot_name: PFlow_ext-hybrid

# Dictfile for the scaling and shifting (json)
dict_file: "/share/rcifdata/srettie/FTAG_preprocessing/full_dips_loose/b-Tagging/scale_dicts/PFlow-scale_dict-22M.json"

# cut definitions to be applied to remove outliers
# possible operators: <, ==, >, >=, <=
cuts:
  JetFitterSecondaryVertex_mass:
    operator: <
    condition: 25000
    NaNcheck: True
  JetFitterSecondaryVertex_energy:
    operator: <
    condition: 1e8
    NaNcheck: True
  JetFitter_deltaR:
    operator: <
    condition: 0.6
    NaNcheck: True
  softMuon_pt:
    operator: <
    condition: 0.5e9
    NaNcheck: True
  softMuon_momentumBalanceSignificance:
    operator: <
    condition: 50
    NaNcheck: True
  softMuon_scatteringNeighbourSignificance:
    operator: <
    condition: 600
    NaNcheck: True
